﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 40f;
    public Rigidbody rb;

    float width;
    float height;

    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        width = (float)Screen.width / 4f;   
    }

    
    void Update()
    {      
        rb.AddForce(Vector3.forward * speed * Time.deltaTime);

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved)
            {
                Vector2 pos = touch.position;
                pos.x = (pos.x - width) / width;
                
                Vector3 vec = new Vector3(pos.x, transform.position.y, transform.position.z);
                transform.position = vec;
            }
        }
    }
}
